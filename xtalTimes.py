
#define findTxtal function
def findtXtal (threshold, xList):
	print ('find txtal, threshold = ', threshold)
	i = (Nframes -1)	#start at the last item in the list
	tXtal = 0
	for item in xList: 
		n = xList[i]	#call items in list starting at bottom, n is amount of crystal at that point	
		if n < threshold:
			tXtal = i+1
			print ('tXtal = ', tXtal)
			return tXtal
		else: 
			i = i -1 


			
			
from sys import argv

if len(argv) < 3: 
	raise standardError % "syntax: xtalTimes.py filename nframes "
			
infilename = argv[1]
Nframes = int(argv[2])
		
#open output file
OutFileName="xtalTimes.dat"
OutFile=open(OutFileName, 'w')
OutFile.write(" \t 30% \t 40% \t 50% \n")

#first, open correct file
Infile = open(infilename, 'r')
print ('opening file: ', infilename)

#initialise values for x30 etc
x20 = -1
x30 = -1
x40 = -1
x50 = -1

#fill up list with xtal values
LineNum=0
xList = [] 
for Line in Infile:
	if LineNum > 0 and LineNum < (Nframes+1) :	#101 for BD, 102 for MD
		Line = Line.strip('\n')
		ElementList = Line.split('\t')
		
		FrameNo = int(ElementList[0])
		TotalX = int(ElementList[12])
		#print TotalX
		xList.append(TotalX)
	
	LineNum = LineNum+1		#end for loop
	
maxXtal = float(max(xList))	
if maxXtal >= 0.5: 
	threshold = 0.5
	x50 = findtXtal(threshold, xList)
if maxXtal >=0.4:
	threshold = 0.4
	x40 = findtXtal(threshold, xList)
if maxXtal >= 0.3:
	threshold = 0.3
	x30 = findtXtal(threshold, xList)
if maxXtal >= 0.2:
	threshold = 0.2
	x20 = findtXtal(threshold, xList)
else: 
	print "simulation does not crystallise"
	
OutputString = "d%i \t %i \t %i \t %i \t %i \n" % (n, x20, x30, x40, x50)
OutFile.write(OutputString)
OutFile.close()
Infile.close()
	

	
	
	

