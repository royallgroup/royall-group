Collection of scripts for converting files to different formats
==============================================================

In this folder you shall find useful scripts for the conversion of ASCII files into different formats, used by several analysis codes.

Everey script contains an explanatory header. Here we briefly describe their purposes.


#convertforBop.py

This Python script converts XYZ coordinate files to a format suitable for the use with Wolfgang Lechner and Cristoph Dellago's [code](http://homepage.univie.ac.at/wolfgang.lechner/bondorderparameter.html) for the local bond order parameter calculation.