import numpy as np
import sys
xyz=np.loadtxt(sys.argv[1],skiprows=2,usecols=[1,2,3])
fhd=open(sys.argv[1]+"BOP",'w')
fhd.write(str(xyz.shape[0])+"\n")
fhd.write("15.0568844\n15.0568844\n12.1035808\n")
np.savetxt(fhd, xyz)
fhd.close()