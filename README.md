# Welcome #

In this repository we store all the pieces of software used within the group in different research areas.

Here we briefly describe the content of the several folders.

##DynamO\_tools

In this folder we collect useful scripts for the data analysis of configurations produced using the M. Bannerman Event Driven molecular dynamics software [DynamO](http://dynamomd.org).

##File\_conversions

The _File\_conversions_ folder contains useful scripts for the conversion of ASCII files between different formats.

##Utilities

In the _Utilities_ folder we provide additional every-day-use small utilities, designed for the execution of simple tasks.