import sys, pylab as pl
from scipy.spatial.distance import cdist

# This script loads several individual configuration files
# (Probably it would be much faster if I made it load all the configurations at once...)
# In order to make it work, then, I suggest you to run this simple command on your 
# xmol trajectory files:
# split -d -a 4 -l 2050 main.xmol 
# This will produce a file of length 2050 for every single configuration,
# named numerically (the -d option) with 4 digits


def Overlap(rt,r0,a):
    # it barely computes the sum over all the distance
    # WARNING:  no normalization
    return pl.sum(cdist(rt, r0) < a)

def Q(t0,t1,dt,prefix,series): 
    # load the first, reference coordinates
    r0=pl.loadtxt(prefix+"%04d.xyz"%t0, usecols=[1,2,3], skiprows=2)
    # we will store the values in here:
    O=[]  
    # this is the time series we are interested in
    ts=series+t0
    print ts
    for t in ts:
        # load new configuration
        rt=pl.loadtxt(prefix+"%04d.xyz"%t, usecols=[1,2,3], skiprows=2)
        # compute the overlap and store it
        O.append(Overlap(rt,r0,0.3))
    return pl.array(O)

def Chi4(table):
    # return the variance
     # WARNING:  no normalization
    return pl.var(table, axis=0)


#  granularity of the time series
step=10
# duration of the time series
time_length=1000 
# frequency of resampling
freq=10
# last configuration to be used as a reference
last=100
# logarithmic series,with added 0 and only integers vaues !
series=pl.concatenate(([0],pl.logspace(1,12, endpoint=False, base=2, num=12).astype('int'),[3000]))
# if you want a linear time series, comment the previous and uncomment the  following line
# series=range(t0,t1,dt)

print "t0 = 0"
# compute the first Q(t)
qt= Q(0, time_length, step, 'corrected',series)
# keep on with the others
for i in range(freq,last,freq):
    print "t0 =",i
    _qt= Q(i, i+time_length, step, 'corrected',series)
    # composing a table (a vertical stack) of the time series
    # every line is a series
    qt=pl.vstack((qt,_qt))
    print qt.shape
# compute the average...
Average_Qt=pl.mean(qt,axis=0) 
# ...and the variance
Xi4=Chi4(qt)
# save
pl.savetxt("ChiAndQ.txt", zip(series+1,Xi4,Average_Qt))
