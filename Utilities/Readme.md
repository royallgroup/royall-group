# Description of the folder content

The folder contains several useful scripts, automatizing simple operations:

## carnahan_startling.py

This Python script computes (and eventually plots) the Carnahan-starlong equation of state for [hard spheres](http://www.sklogwiki.org/SklogWiki/index.php/Hard_sphere_model)