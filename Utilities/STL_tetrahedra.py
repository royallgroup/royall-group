import numpy 


# Facet orientation.  The facets define the surface of a 3-dimensional object.  As such, each facet is part of the boundary between the interior and the exterior of the object.  The orientation of the facets  is specified redundantly in two ways which must be consistent.  First, the direction of the normal is outward.  Second, the vertices are listed in counterclockwise order when looking at the object from the outside (right-hand rule). 

# Vertex-to-vertex rule.  Each triangle must share two vertices with each of its adjacent triangles.  In other words, a vertex of one triangle cannot lie on the side of another.    
def write_facet(fhd,vertex1,vertex2,vertex3):
    edge1=vertex2-vertex1
    edge2=vertex3-vertex2
    normal=numpy.cross(edge1, edge2)
    normal/=numpy.linalg.norm(normal)
    fhd.write("   facet normal %e %e %e\n"%(normal[0], normal[1] , normal[2]))
    fhd.write("      outer loop\n")
    fhd.write("         vertex %e %e %e\n"%(vertex1[0],vertex1[1],vertex1[2]))
    fhd.write("         vertex %e %e %e\n"%(vertex2[0],vertex2[1],vertex2[2]))
    fhd.write("         vertex %e %e %e\n"%(vertex3[0],vertex3[1],vertex3[2]))
    fhd.write("      endloop\n")
    fhd.write("   endfacet\n")
def write_tetrahedron(fhd,p1, p2, p3,p4):
    # vertices are given in 
    write_facet(fhd, p1, p2, p4)
    write_facet(fhd, p4, p2, p3)
    write_facet(fhd, p3, p2, p1)
    write_facet(fhd, p1, p4, p3)
# write STL file
filename="my.stl"
fhd=open(filename, 'w')
# write header
fhd.write("solid tetrahedra\n")


# edge length
Lx=1.
Ly=numpy.sqrt(3)/2.*Lx
Lz=numpy.sqrt(13)/4*Lx
Ni=10
Nj=10
dx=numpy.array([Lx,0,0])
dy=numpy.array([0,Ly,0])

# for i in range(Ni):
#     for j in range(Nj):
#         particle1=numpy.array([0,0,0])+dx*i+dy*j
#         particle2=numpy.array([Lx,0,0])+dx*i+dy*j
#         particle3=numpy.array([Lx/2.,Ly,0])+dx*i+dy*j
#         particle4=numpy.array([0.5*Lx,Ly/2.,Lz])+dx*i+dy*j
#         write_tetrahedron(fhd,particle1,particle2,particle3,particle4)
for i in range(Ni):
    for j in range(Nj):
        particle1=numpy.array([0,0,0])+dx*i+dy*j
        particle2=numpy.array([Lx,0,0])+dx*i+dy*j
        particle3=numpy.array([Lx/2.,Ly,0])+dx*i+dy*j
        particle4=numpy.array([0.5*Lx,Ly/2.,Lz])+dx*i+dy*j
        write_tetrahedron(fhd,particle1,particle2,particle3,particle4)
        # particle1+=numpy.array([2*Lx,Ly,0])
        # particle4+=numpy.array([Lx,0,0])
        # write_tetrahedron(fhd,particle1,particle2,particle3,particle4)


fhd.write("endsolid\n")
fhd.close()

