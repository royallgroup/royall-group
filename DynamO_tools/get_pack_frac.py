#!/usr/local/bin/python
#Load an xml library
import xml.etree.ElementTree as ET
#load a random number generator
import random,os
random.seed() #Always seed your generator!
import sys

def loadXMLFile(filename):
    #Check if the file is compressed or not, and 
    if (os.path.splitext(filename)[1][1:].strip() == "bz2"):
        import bz2
        f = bz2.BZ2File(filename)
        doc = ET.parse(f)
        f.close()
        return doc
    else:
        return ET.parse(filename)
#Load the XML file
XMLFile = loadXMLFile(sys.argv[1])
Lx=float(XMLFile.find(".//SimulationSize").get("x"))
Ly=float(XMLFile.find(".//SimulationSize").get("y"))
Lz=float(XMLFile.find(".//SimulationSize").get("z"))
Volume=Lx*Ly*Lz
FilledVolume=0
values=[]

from numpy import pi
#Add the diameter and mass tags
for ParticleTag in XMLFile.findall("./ParticleData/Pt"):
    #Add attributes for the diameter
    D=float(ParticleTag.get("D"))
    volume=4./3.*pi*(D/2)**3
    values.append(volume)
    FilledVolume+=volume

print "The packing fraction is", FilledVolume/Volume

